import React from 'react'
import ReactDOM from 'react-dom'
import App from './App.jsx'

// const App = () => <h1>Hola desde webpack</h1>;

ReactDOM.render(<App />, document.getElementById('app'))
