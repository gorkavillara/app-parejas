import React from 'react'
import './App.css'

// JSDoc

/**
 *
 * @function suma
 * @desc La función suma se utiliza en el componente App para devolver la suma entre el primer factor y segundo factor
 * @param {number} a Primer factor
 * @param {number} b Segundo factor
 * @returns La suma de a + b
 */
function suma (a, b) { // eslint-disable-line no-unused-vars
  return a + b
}

/**
 *
 * @returns El componente App
 */
const App = () => <h1>Componente App</h1>

export default App
